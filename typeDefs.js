const typeDefs = `

type Admin {
    id: ID!
    name: String
    email: String
    phoneNumber: String
    password: String
    username: String   
    role: [String]
    photoUrl: String
    addedBy :Admin
    createdAt: String
    updatedAt: String

    rights:[Right]
    vehicles:[Vehicle]
}

type Booking {
    id: ID!
    tripId: Trip
    seatNo: String  
    passenger: Passenger
    payment: Payment
    createdAt: String
    updatedAt: String
}

type Passenger {
    name: String
    age: Int
    idNumber: String
    gender: String
    email: String
    phoneNumber: String
}

type Payment {
    tx_ref: String
    timestamp: Int   
    mode: String  
}

type Log {
    id: ID!
    action: String
    executioner: Admin
    createdAt: String
    updatedAt: String
}

type Right {
    id: ID!
    label: String
    authorised: [Admin]
}

type Route {
    id: ID!
    departure: String
    destination: String
    addedBy: Admin
    fare: Int
    removed: Boolean
    duration: Int
    createdAt: String
    updatedAt: String

    trips: [Trip]
    vehicles: [Vehicle]
}

type Trip {
    id:ID!
    route : Route    
    departureTime: String
    journeyType: String
    vehicle: Vehicle
    driver: Admin
    cancelled: Boolean
    createdBy: Admin
    createdAt: String
    updatedAt: String

    bookings: [Booking]
}

type User {
    id: ID!
    name: String
    email: String
    phoneNumber: String
    password: String
    age: Int
    gender: String
    idNumber: String
    photoUrl: String
    createdAt: String
    updatedAt: String
}

type Vehicle {
    id: ID!
    registration: String
    primaryRoute: Route
    owner: Admin
    driver: Admin
    addedBy: Admin
    removed: Boolean
    createdAt: String
    updatedAt: String
    seats: Int

    trips: [Trip]
}

type Query {
    getVehiclesCount:Int
    getTripsCount:Int
    getUsersCount:Int
    getAdmin(id:ID!): Admin
    getAdminsCount:Int
    getLogs:[Log]
    getRights: [Right]
    getAdmins: [Admin]
    getTrips:[Trip]
    getVehicles:[Vehicle]
    getRoutes: [Route]
    getUsers:[User]
}

type Mutation {
    createTrip(route:ID, departureTime:String, journeyType:String, vehicle:ID, driver:ID, createdBy:ID):Trip
    bookTicket(trip:ID, name:String, age:Int, gender:String, phoneNumber:String, tx_ref:String, timestamp:String):Booking
    addRoute(departure:String, destination:String, addedBy:ID, fare:Int, duration:Int ):Route
    addVehicle(driver:ID, route:ID, registration:String, owner:ID, addedBy:ID , seats:Int):Vehicle
    addUser:User
    addRight(label:String):Right
    addAdmin(password:String, name:String, email:String, phoneNumber:String, username:String, role:String!, photoUrl:String, addedBy:ID , rightIds: String):Admin
    addLog:Log
    authorise(admin:ID,right:ID): Admin

    updateTrip(id:ID, driver:ID, vehicle:ID, departureTime:String):Trip
    updateVehicle(id:ID, driver:ID, primaryRoute:ID, removed: Boolean):Vehicle
    updateUser:[User]
    updateRight:[Right]
    updateRoute(id:ID!, fare:Int , removed:Boolean):Route
    
}

`;

export default typeDefs;
