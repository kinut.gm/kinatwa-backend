import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const LogSchema = new Schema(
  {
    action: { type: String },
    executioner: { type: Schema.Types.ObjectId, ref: "Admin" },
  },
  {
    collection: "logs",
  }
);

LogSchema.plugin(timestamps);

LogSchema.index({ createdAt: 1, updatedAt: 1 });

export const Log = mongoose.model("Log", LogSchema);
