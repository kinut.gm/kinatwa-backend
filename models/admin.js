import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const AdminSchema = new Schema(
  {
    name: { type: String },
    email: { type: String },
    phoneNumber: { type: String },
    password: { type: String },
    username: { type: String },
    role: [{ type: String }],
    photoUrl: { type: String },
    addedBy: { type: Schema.Types.ObjectId, ref: "Admin" },
  },
  {
    collection: "admins",
  }
);

AdminSchema.plugin(timestamps);

AdminSchema.index({ createdAt: 1, updatedAt: 1 });

export const Admin = mongoose.model("Admin", AdminSchema);
