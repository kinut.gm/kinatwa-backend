import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const RouteSchema = new Schema(
  {
    departure: { type: String },
    destination: { type: String },
    addedBy: { type: Schema.Types.ObjectId, ref: "Admin" },
    fare: { type: Number },
    removed: { type: Boolean, default: false },
    duration: { type: Number },
  },
  {
    collection: "routes",
  }
);

RouteSchema.plugin(timestamps);

RouteSchema.index({ createdAt: 1, updatedAt: 1 });

export const Route = mongoose.model("Route", RouteSchema);
