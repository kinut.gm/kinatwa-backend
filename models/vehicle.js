import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const VehicleSchema = new Schema(
  {
    registration: { type: String },
    primaryRoute: { type: Schema.Types.ObjectId, ref: "Route" },
    owner: { type: Schema.Types.ObjectId, ref: "Admin" },
    driver: { type: Schema.Types.ObjectId, ref: "Admin" },
    addedBy: { type: Schema.Types.ObjectId, ref: "Admin" },
    removed: { type: Boolean, default: false },
    seats: { type: Number },
  },
  {
    collection: "vehicles",
  }
);

VehicleSchema.plugin(timestamps);

VehicleSchema.index({ createdAt: 1, updatedAt: 1 });

export const Vehicle = mongoose.model("Vehicle", VehicleSchema);
