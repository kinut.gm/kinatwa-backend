import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

export const BookingSchema = new Schema(
  {
    tripId: { type: Schema.Types.ObjectId, ref: "Trip" },
    seatNo: { type: String },
    passenger: {
      name: { type: String },
      age: { type: Number },
      idNumber: { type: String },
      gender: { type: String },
      email: { type: String },
      phoneNumber: { type: String },
    },
    payment: {
      tx_ref: { type: String },
      timestamp: { type: Number },
      mode: { type: String },
    },
  },
  {
    collection: "bookings",
  }
)

BookingSchema.plugin(timestamps)

BookingSchema.index({ createdAt: 1, updatedAt: 1 })

export const Booking = mongoose.model("Booking", BookingSchema)
