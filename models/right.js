import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const RightSchema = new Schema(
  {
    label: { type: String },
    authorised: [{ type: Schema.Types.ObjectId, ref: "Admin" }],
  },
  {
    collection: "rights",
  }
);

RightSchema.plugin(timestamps);

RightSchema.index({ createdAt: 1, updatedAt: 1 });

export const Right = mongoose.model("Right", RightSchema);
