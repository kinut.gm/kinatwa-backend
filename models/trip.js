import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const TripSchema = new Schema(
  {
    route: { type: Schema.Types.ObjectId, ref: "Route" },
    departureTime: { type: String },
    journeyType: { type: String },
    vehicle: { type: Schema.Types.ObjectId, ref: "Vehicle" },
    driver: { type: Schema.Types.ObjectId, ref: "Admin" },
    cancelled: { type: Boolean, default: false },
    createdBy: { type: Schema.Types.ObjectId, ref: "Admin" },
  },
  {
    collection: "trips",
  }
);

TripSchema.plugin(timestamps);

TripSchema.index({ createdAt: 1, updatedAt: 1 });

export const Trip = mongoose.model("Trip", TripSchema);
