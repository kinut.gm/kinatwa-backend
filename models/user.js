import mongoose from "mongoose";
import timestamps from "mongoose-timestamp";

const { Schema } = mongoose;

export const UserSchema = new Schema(
  {
    name: { type: String },
    email: { type: String },
    phoneNumber: { type: String },
    password: { type: String },
    age: { type: Number },
    gender: { type: String },
    idNumber: { type: String },
    photoUrl: { type: String },
  },
  {
    collection: "users",
  }
);

UserSchema.plugin(timestamps);

UserSchema.index({ createdAt: 1, updatedAt: 1 });

export const User = mongoose.model("User", UserSchema);
