import { Booking } from "./models/booking.js";
import { Trip } from "./models/trip.js";
import { Vehicle } from "./models/vehicle.js";
import { User } from "./models/user.js";
import { Admin } from "./models/admin.js";
import { Route } from "./models/route.js";
import { Log } from "./models/log.js";
import { Right } from "./models/right.js";

const resolvers = {
  Admin: {
    rights: async (parent, args) => {
      let rights = await Right.find({ authorised: parent.id });
      return rights;
    },

    vehicles: async (parent, args) => {
      let vehicles = await Vehicle.find({ owner: parent.id });
      return vehicles;
    },
  },

  Trip: {
    driver: async (parent, args) => {
      let driver = await Admin.findById(parent.driver);
      return driver;
    },
    vehicle: async (parent, args) => {
      let vehicle = await Vehicle.findById(parent.vehicle);
      return vehicle;
    },
    route: async (parent, args) => {
      let route = await Route.findById(parent.route);
      return route;
    },
    createdBy: async (parent, args) => {
      let creator = await Admin.findById(parent.createdBy);
      return creator;
    },
    bookings: async (parent, args) => {
      let bookings = await Booking.find({ tripId: parent.id });
      return bookings;
    },
  },

  Route: {
    trips: async (parent, args) => {
      let trips = await Trip.find({ route: parent.id });
      return trips;
    },
    vehicles: async (parent, args) => {
      let vehicles = await Vehicle.find({ primaryRoute: parent.id }).populate(
        "driver"
      );
      return vehicles;
    },
  },

  Vehicle: {
    driver: async (parent, args) => {
      let driver = await Admin.findById(parent.driver);
      return driver;
    },
    trips: async (parent, args) => {
      let trips = await Trip.find({ vehicle: parent.id });
      return trips;
    },
    primaryRoute: async (parent, args) => {
      let route = await Route.findById(parent.primaryRoute);
      return route;
    },
  },

  Query: {
    getAdmin: async (_, args) => {
      let user = await Admin.findById(args.id).populate("addedBy");
      return user;
    },
    getVehiclesCount: async () => {
      let count = await Vehicle.countDocuments();
      return count;
    },
    getRoutes: async () => {
      let routes = await Route.find();
      return routes;
    },
    getTrips: async () => {
      let trips = await Trip.find();
      return trips;
    },
    getRights: async () => {
      let rights = await Right.find().populate("authorised");
      return rights;
    },
    getAdmins: async () => {
      let admins = await Admin.find();
      return admins;
    },
    getVehicles: async () => {
      let vehicles = await Vehicle.find()
        .populate("primaryRoute")
        .populate("owner")
        .populate("driver")
        .populate("addedBy");

      return vehicles;
    },
    getTripsCount: async () => {
      let count = await Trip.countDocuments();
      return count;
    },
    getUsersCount: async () => {
      let count = await User.countDocuments();
      return count;
    },
    getAdminsCount: async () => {
      let count = await Admin.countDocuments();
      return count;
    },
  },

  Mutation: {
    createTrip: async (_, args) => {
      const { route, journeyType, departureTime, createdBy, vehicle, driver } =
        args;

      let newTrip = new Trip({
        route,
        departureTime,
        journeyType,
        vehicle,
        driver,
        createdBy,
      });

      let trip = await newTrip.save();

      // Create bookings
      Vehicle.findById(vehicle).then((vehicle) => {
        for (let i = 0; i < vehicle.seats; i++) {
          let newBooking = new Booking({
            tripId: String(trip.id),
            seatNo: String(i + 1),
            payment: {},
          });

          newBooking.save().then((booking) => console.log(booking));
        }
      });

      // Log creation
      // let newLog = new Log({
      //   action: `created a new trip. Destined to arrive in ${
      //     trip.route.destination
      //   } from ${trip.route.departure} at ${new Date(
      //     parseInt(trip.route.duration)
      //   ).toDateString()}. `,
      //   executioner: createdBy,
      // });

      // newLog.save();

      // Return trip
      return trip;
    },

    bookTicket: async (_, args) => {
      const { trip } = args;

      function omit(obj, ...props) {
        const result = { ...obj };
        props.forEach(function (prop) {
          delete result[prop];
        });
        return result;
      }

      if (JSON.stringify(omit(args, "trip")) === "{}") {
        let booking = await Booking.findOneAndUpdate(
          {
            tripId: trip,
            payment: null,
          },
          {
            passenger: {
              name: "CUSTOMER",
              age: 18,
              gender: "M/F",
              idNumber: "*******",
              phoneNumber: "0**********",
            },
            payment: {
              mode: "CASH",
              timestamp: Date.now(),
            },
          },
          {
            new: true,
          }
        );

        return booking;
      }
    },

    addRight: async (_, args) => {
      const { label } = args;

      let newRight = new Right({
        label,
      });

      let right = newRight.save();

      return right;
    },

    addRoute: async (_, args) => {
      const { departure, destination, addedBy, fare, duration } = args;

      let newRoute = new Route({
        departure,
        destination,
        addedBy,
        fare,
        duration,
      });

      let route = newRoute.save();

      return route;
    },

    addVehicle: async (_, args) => {
      const { driver, route, registration, owner, addedBy, seats } = args;

      let newVehicle = new Vehicle({
        registration,
        owner,
        driver,
        addedBy,
        primaryRoute: route,
        seats: Number(seats),
      });

      let vehicle = newVehicle.save();

      return vehicle;
    },

    addAdmin: async (_, args) => {
      const {
        name,
        email,
        phoneNumber,
        username,
        role,
        photoUrl,
        addedBy,
        rightIds,
        password,
      } = args;

      let newAdmin = new Admin({
        name,
        email,
        phoneNumber,
        username,
        photoUrl,
        addedBy,
        password: password ? password : "kinatwasacco",
        role: role.split(","),
      });

      let admin = await newAdmin.save();

      if (rightIds.split(",").length > 0 && rightIds.length > 5) {
        console.log(rightIds.split(",").length);
        rightIds.split(",").map(async (id) => {
          await Right.updateOne(
            { _id: id },
            { $addToSet: { authorised: admin.id } }
          );
          console.log(admin.id);
        });
        return;
      }

      return admin;
    },

    authorise: async (_, args) => {
      const { admin, right } = args;

      await Right.updateOne(
        { id: right },
        {
          $addToSet: { authorised: admin },
        }
      );

      let _admin = await Admin.findById(admin);
      return _admin;
    },

    updateRoute: async (_, args) => {
      const { id, removed, fare } = args;

      function omit(obj, ...props) {
        const result = { ...obj };
        props.forEach(function (prop) {
          delete result[prop];
        });
        return result;
      }

      await Route.updateOne({ id: id }, omit(args, id));

      let route = await Route.findById(id);
      return route;
    },

    updateVehicle: async (_, args) => {
      const { id } = args;

      console.log(args);

      function omit(obj, ...props) {
        const result = { ...obj };
        props.forEach(function (prop) {
          delete result[prop];
        });
        return result;
      }

      await Vehicle.updateOne({ id: id }, omit(args, id));

      let vehicle = await Vehicle.findById(id);
      return vehicle;
    },

    updateTrip: async (_, args) => {
      const { id } = args;

      console.log(args);

      function omit(obj, ...props) {
        const result = { ...obj };
        props.forEach(function (prop) {
          delete result[prop];
        });
        return result;
      }

      await Trip.updateOne({ id: id }, omit(args, id));

      let trip = await Trip.findById(id);
      return trip;
    },
  },
};

export default resolvers;
